const argv = require('./config/yargs').argv;
const colors = require('colors/safe');
const todos = require('./to-do/to-do');

//console.log(argv);
let comando = argv._[0];

switch (comando) {
    case 'crear':
        console.log('crear una tarea');

        let todo = todos.crear(argv.descripcion);
        console.log(todo);
        break;

    case 'editar':
        console.log('editar una tarea');

        let actualizado = todos.editar(argv.descripcion, argv.completado);
        console.log(actualizado);

        break;

    case 'borrar':

        let borrado = todos.borrar(argv.descripcion);
        console.log(borrado);
        break;

    case 'listar':
        console.log('mostrar todas las tareas');

        let elementos = todos.listar();

        console.log(colors.rainbow('-----Lista de tareas-----'));
        for (const elemento of elementos) {
            console.log(colors.yellow('---Tarea---'));
            console.log(elemento.descripcion);
            console.log('Estado:', elemento.completado);
            console.log(colors.yellow('----------'));
        };

        break;

    default:
        console.log('comando no reconocido');

}