const fs = require('fs');

let todos = [];

const guardarDB = () => {
    let data = JSON.stringify(todos);

    fs.writeFile('db/data.json', data, (err) => {
        if (err) throw new Error(`No se pudo guardar: ${err}`);
    })
}

const cargarDB = () => {
    try {
        todos = require('../db/data.json');
    } catch (error) {
        todos = [];
    }

}

const crear = (descripcion) => {

    cargarDB();

    let todo = {
        descripcion,
        completado: false,
        fecha: new Date()
    }

    todos.push(todo);

    guardarDB();

    return todo;
}

const editar = (descripcion, completado = true) => {
    cargarDB();

    let index = todos.findIndex(tarea => {
        return tarea.descripcion === descripcion;
    });

    if (index >= 0) {
        todos[index].completado = completado;
        guardarDB();
        return true;
    } else {
        return false;
    }
}

const borrar = (descripcion) => {
    cargarDB();

    let filtrados = todos.filter(tarea => {
        return tarea.descripcion !== descripcion;
    });

    if (filtrados.length !== todos.length) {
        todos = filtrados;
        guardarDB();
        return true;
    } else {
        return false;
    }


}

const listar = () => {

    cargarDB();

    return todos;
}

module.exports = {
    crear,
    editar,
    borrar,
    listar
}