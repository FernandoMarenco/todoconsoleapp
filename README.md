# Todo console app

Esta es una aplicación donde se utiliza la consola para crear una lista de tareas.

Comando: ```crear```

```--descripcion, -d='descripción de la tarea'``` => para crear una nueva tarea (obligatorio)


Comando: ```editar```

```--descripcion, -d='descripción de la tarea'``` => para buscar una tarea (obligatorio)

```--completado, -c=boolean``` => para marcar como completado una tarea (opcional si es true)


Comando: ```borrar```

```--descripcion, -d='descripción de la tarea'``` => para borrar una tarea (obligatorio)


Comando: ```listar```

Listar todas las tareas existentes


## Ejemplo

Crear el todo: Hacer la tarea

```
node app crear -d 'Hacer la tarea'
```


```npm install```
Para instalar los node_modules