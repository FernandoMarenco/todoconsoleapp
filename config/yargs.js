const descripcion = {
    demand: true,
    alias: 'd',
    desc: 'Descripción de la tarea'
};

const completado = {
    default: true,
    alias: 'c',
    desc: 'Estado de la tarea'
};


const argv = require('yargs')
    .command('crear', 'Crear una tarea nueva', {
        descripcion
    })
    .command('editar', 'Editar una tarea', {
        descripcion,
        completado
    })
    .command('borrar', 'Borrar una tarea', {
        descripcion
    })
    .help()
    .argv;

module.exports = {
    argv
}